#!/usr/bin/env python3

import shutil
import os
import glob
import sys
import subprocess

namePassFile = 'pass.txt'

if not os.path.exists(namePassFile):
    print('Create endfile and passfile')
    sys.exit()

savedPath = os.getcwd()

def zipping(password, name):
    artype = 'zip'
    #artype = '7z -mhe=on'
    if password: cmd = '7z a -t%s -p"%s" ../%s' % (artype,password, name)
    else: cmd = '7z a -t%s ../%s' % (artype, name)
    if sys.platform == 'win32': subprocess.check_output(savedPath+'\\7z\\'+cmd, shell=True)
    else: subprocess.check_output(cmd, shell=True)

allpass = []

with open(namePassFile, 'r') as f:
    allpass = [c.rstrip('\r\n') for c in f.readlines()]
    
# генерируем путь
path = '/'.join(list(map(str, list(range(len(allpass)+1)))))

# создаем дирректории
os.makedirs(path)

# копируем конечный файл
if os.path.exists('./end/'):
    for f in glob.glob('./end/*.*'):
        shutil.copy(f, path)

# переходим в самый конец
os.chdir(path)
namepass = allpass.pop()

while namepass:
    name = os.path.basename(os.getcwd())
    
    # копируем из песочницы файлы
    fsfile = glob.glob(os.path.join(savedPath, 'sand/') + namepass + '.*')
    fsfile = fsfile[0] if len(fsfile) else sys.exit(namepass + ' not correct')
    shutil.copy(fsfile, '../'+ name + fsfile[fsfile.find('.'):])
    
    # упаковываем
    zipping(namepass, name)
    print('%s.zip - %s' % (name, namepass))
    namepass = allpass.pop() if len(allpass) else 0
    os.chdir('..')
    if os.path.exists(name): shutil.rmtree(name)

zipping('', 'quest')
name = os.path.basename(os.getcwd())
os.chdir('..')
shutil.rmtree(name)
print('Finished! File - quest.zip')
input()
