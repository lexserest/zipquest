import sys, os, math
from PIL import Image, ImageDraw 

if len(sys.argv)!=2:
    sys.exit('Использование: img2text filepath')

path = sys.argv[1]
data = None
 
if os.path.exists(path):
    if path.endswith('.png'): image = Image.open(path)
    else: 
        with open(path, 'rb') as f: data = f.read()
        size = round(math.ceil(math.sqrt(len(data)/3)))
        image = Image.new("RGB", (size, size), (0,0,0))
else: sys.exit('error args')

draw = ImageDraw.Draw(image) 
width = image.size[0] 
height = image.size[1]  
pix = image.load() 

if path.endswith('.png'):
    with open('out', 'wb') as f:
        data = []
        for i in range(width):
            for j in range(height):
                data.append(pix[j, i][0])
                data.append(pix[j, i][1])
                data.append(pix[j, i][2])
        f.write(bytes(data))
    print('Сохранено в out')

else:
    data = list(data)
    for i in range(width):
        for j in range(height):
            c1 = data.pop(0) if len(data) else 0
            c2 = data.pop(0) if len(data) else 0
            c3 = data.pop(0) if len(data) else 0
            image.putpixel((j, i), (c1,c2,c3))

    image.save('out.png', "PNG")
    print('Сохранено в out.png')

