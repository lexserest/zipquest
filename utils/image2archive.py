import glob, sys

img = glob.glob('*.png')
img += glob.glob('*.jpg')
if len(img)!=1: sys.exit('Only 1 image')

zip = glob.glob('*.zip')
zip += glob.glob('*.7z')
zip += glob.glob('*.rar')
if len(zip)!=1: sys.exit('Only 1 archive')

img = img[0]
zip = zip[0]

with open(img,'rb') as f1, open(zip,'rb') as f2, open('out' + img[img.find('.'):], 'wb') as out:
    while True:
        bytes=f1.read(1024)
        if bytes: n=out.write(bytes)
        else: break
    while True:
        bytes=f2.read(1024)
        if bytes: n=out.write(bytes)
        else: break
print('Finish!')
